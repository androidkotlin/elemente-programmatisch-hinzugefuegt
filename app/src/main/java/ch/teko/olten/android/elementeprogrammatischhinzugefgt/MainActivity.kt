package ch.teko.olten.android.elementeprogrammatischhinzugefgt

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.RadioButton
import android.widget.RadioGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val antworten = RadioGroup(this)
        antworten.addView(buildAntwortRadioButton("Gut"))
        antworten.addView(buildAntwortRadioButton("Mittel"))
        antworten.addView(buildAntwortRadioButton("Schlecht"))

        antwortLayout.addView(antworten)

        val klickButton = Button(this)
        klickButton.text = "Klick!"

        antwortLayout.addView(klickButton)

        klickButton.setOnClickListener {
            val ausgewaehlteAntwortId = antworten.checkedRadioButtonId
            val ausgewaehlteAntwortRadioButton = findViewById<RadioButton>(ausgewaehlteAntwortId)

            Toast.makeText(this, "Klick: ${ausgewaehlteAntwortRadioButton.tag}", Toast.LENGTH_LONG).show()
        }
    }

    private fun buildAntwortRadioButton(antwort: String): RadioButton {
        val antwortButton = RadioButton(this)
        antwortButton.id = View.generateViewId()
        antwortButton.text = antwort
        antwortButton.tag = "$antwort Button"
        return antwortButton
    }
}
